using UnityEngine;
using System.Collections;

public class BlockMoveY : MonoBehaviour
{

    private float speedY = 1;

    void Update()
    {
	    transform.Translate (0, speedY * Time.deltaTime, 0, Space.World);
	
	    if (transform.position.y >= 1)
	    {
		    speedY *= -1;
	    }
	
	    if (transform.position.y <= -6)
	    {
		    speedY *= -1;
	    }
    }
}