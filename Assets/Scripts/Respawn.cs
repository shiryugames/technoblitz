using UnityEngine;
using System.Collections;

public class Respawn : MonoBehaviour
{

    public GameObject respawn;

    void Start()
    {
	    transform.position = respawn.transform.position;
    }

    void OnTriggerEnter(Collider collider)
    {
	    if (collider.tag == "Death")
	    {
		    transform.position = respawn.transform.position;
	    }
    }
}