using UnityEngine;
using System.Collections;

public class BlockMoveZ : MonoBehaviour
{

    private float speedZ = 2;

    void Update()
    {
	    transform.Translate (0, 0, speedZ * Time.deltaTime, Space.World);
	
	    if (transform.position.z <= 54)
	    {
		    speedZ *= -1;
	    }
	
	    if (transform.position.z >= 68)
	    {
		    speedZ *= -1;
	    }
    }
}