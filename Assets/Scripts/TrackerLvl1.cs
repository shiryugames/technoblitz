using UnityEngine;
using System.Collections;

public class TrackerLvl1 : MonoBehaviour
{

    public GameObject playerTrack;
    public GameObject tracker;

    private Vector3 temp_pos;

    void Update()
    {
	    transform.position = playerTrack.transform.position;
	
	    if (playerTrack.transform.position.z >= 6)
	    {
		    //transform.position.y -= 6;
            //transform.position.z += 10;

            temp_pos = transform.position;
            temp_pos.y -= 6;
            temp_pos.z += 10;
            transform.position = temp_pos;
	    }
	
	    if (playerTrack.transform.position.z >= 98 && playerTrack.transform.position.x >= 13)
	    {
		    //transform.position.y += 6;
            //transform.position.z -= 16;

            temp_pos = transform.position;
            temp_pos.y += 6;
            temp_pos.z -= 16;
            transform.position = temp_pos;
	    }
	
	    if (playerTrack.transform.position.y <= -20)
	    {
            //transform.position.z += 6;

            temp_pos = transform.position;
            temp_pos.z += 6;
            transform.position = temp_pos;
	    }
    }
}