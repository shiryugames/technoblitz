/*
Script to help switch between scenes
this will alow you to select the level in the inspector
*/

using UnityEngine;
using System.Collections;

public class MenuQuit : MonoBehaviour
{

    public void Quit()
    {
	    Application.Quit();
    }
}