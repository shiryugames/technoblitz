using UnityEngine;
using System.Collections;

public class Tracker : MonoBehaviour
{

    public GameObject playerTrack;
    public GameObject tracker;

    private Vector3 temp_pos;

    void Update()
    {
	    transform.position = playerTrack.transform.position;
	
	    if (playerTrack.transform.position.z >= 10)
	    {
		    //transform.position.y -= 4;
		    //transform.position.z += 8;

            temp_pos = transform.position;
            temp_pos.y -= 4;
            temp_pos.z += 8;
            transform.position = temp_pos;

	    }
	
	    if (playerTrack.transform.position.z >= 62 && playerTrack.transform.position.x >= 13)
	    {
		    //transform.position.y += 4;
		    //transform.position.z -= 8;

            temp_pos = transform.position;
            temp_pos.y += 4;
            temp_pos.z -= 8;
            transform.position = temp_pos;
	    }
    }
}