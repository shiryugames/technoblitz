using UnityEngine;
using System.Collections;

public class DoorOpen : MonoBehaviour
{

    public GameObject door;

    void OnTriggerExit ()
    {
	    door.GetComponent<Animation>().Play("Door");
    }
}