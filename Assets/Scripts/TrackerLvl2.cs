using UnityEngine;
using System.Collections;

public class TrackerLvl2 : MonoBehaviour
{

    public GameObject playerTrack;
    public GameObject tracker;

    private Vector3 temp_pos;

    void Update()
    {
	    transform.position = playerTrack.transform.position;
	
	    if (playerTrack.transform.position.z >= 5.5f)
	    {
		    //transform.position.y -= 9;
            //transform.position.z += 12;

            temp_pos = transform.position;
            temp_pos.y -= 9;
            temp_pos.z += 12;
            transform.position = temp_pos;
	    }
	
	    if (playerTrack.transform.position.z >= 58)
	    {
		    //transform.position.y += 3;
            //transform.position.z -= 2;

            temp_pos = transform.position;
            temp_pos.y += 3;
            temp_pos.z -= 2;
            transform.position = temp_pos;
	    }
	
	    if (playerTrack.transform.position.z >= 91 && playerTrack.transform.position.x >= 53)
	    {
		    //transform.position.y += 9;
            //transform.position.z -= 10;

            temp_pos = transform.position;
            temp_pos.y += 9;
            temp_pos.z -= 10;
            transform.position = temp_pos;
	    }
	
	    if (playerTrack.transform.position.y >= -24)
	    {
            //transform.position.z += 3;

            temp_pos = transform.position;
            temp_pos.z += 3;
            transform.position = temp_pos;
	    }
	
    }
}