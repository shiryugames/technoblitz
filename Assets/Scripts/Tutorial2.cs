using UnityEngine;
using System.Collections;

public class Tutorial2 : MonoBehaviour
{

    public GameObject text;

    void Start()
    {
	    text.SetActive (false);
    }

    void OnTriggerEnter()
    {
	    text.SetActive (true);
    }

    void Update()
    {
	    if (Input.GetKey(KeyCode.LeftShift))
	    {
		    text.SetActive(false);
		    //Destroy(text);
	    }
    }
}