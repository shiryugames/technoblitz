using UnityEngine;
using System.Collections;

public class TutorialConnect : MonoBehaviour
{

    public GameObject text;

    void Start()
    {
	    text.SetActive(false);
    }

    void OnTriggerEnter()
    {
	    text.SetActive(true);
    }

    void Update()
    {
	    if (Input.GetKey(KeyCode.W) || (Input.GetKey(KeyCode.S)))
	    {
		    text.SetActive(false);
	    }
    }
}