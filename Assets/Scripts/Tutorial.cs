using UnityEngine;
using System.Collections;

public class Tutorial : MonoBehaviour
{

    public GameObject text;

    void Start()
    {
	    text.SetActive(true);
    }

    void Update()
    {
	    if (Input.GetKey(KeyCode.A) || (Input.GetKey(KeyCode.D)))
	    {
		    text.SetActive(false);
		    Destroy(gameObject);
	    }
    }
}