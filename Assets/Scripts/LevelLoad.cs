/*
Script to help switch between scenes
this will alow you to select the level in the inspector
*/

using UnityEngine;
using System.Collections;

public class LevelLoad : MonoBehaviour
{

    public string myLevel; 

    void OnCollisionEnter(Collision myCollision)
    {
	    if (myCollision.gameObject.name == "Space")
	    {
		    Application.LoadLevel(myLevel);
	    }
    }
}