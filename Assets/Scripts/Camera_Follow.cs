﻿using UnityEngine;
using System.Collections;

public class Camera_Follow : MonoBehaviour {

    public Transform target;
    public float distance_x;
    public float distance_y;
    public float distance_z;

    private Vector3 temp_pos;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        temp_pos = target.position;
        temp_pos.x = target.position.x - distance_x;
        temp_pos.y = target.position.y + distance_y;
        temp_pos.z = target.position.z - distance_z;
        transform.position = temp_pos;
	}
}